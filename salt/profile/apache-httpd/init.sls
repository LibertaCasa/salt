{%- set snippetsdir = '/etc/apache2/snippets.d' -%}
{%- set mypillar = salt['pillar.get']('profile:apache-httpd', {}) -%}

{{ snippetsdir }}:
  file.directory:
    - makedirs: True

{%- if 'snippets' in mypillar %}
{%- for snippet, config in mypillar['snippets'].items() %}
{{ snippetsdir }}/{{ snippet }}.conf:
  file.managed:
    - contents:
      {%- for line in config %}
      - {{ line }}
      {%- endfor %}
    - require:
      - file: {{ snippetsdir }}
    {#- formula dependencies #}
    - watch_in:
      - service: apache-service-running
{%- endfor %}
{%- endif %}

include:
  - apache.config



