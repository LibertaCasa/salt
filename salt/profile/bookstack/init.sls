{%- set mypillar = salt['pillar.get']('profile:bookstack', {}) -%}
{%- set configfile = '/etc/sysconfig/BookStack' -%}

bookstack_packages:
  pkg.installed:
    - names:
      - BookStack-config-php-fpm-apache

bookstack_permissions:
  file.managed:
    - mode: '0640'
    - user: root
    - group: www
    - names:
      - {{ configfile }}

{%- if mypillar | length %}
{{ configfile }}:
  file.keyvalue:
    - separator: '='
    - show_changes: False
    - require:
      - pkg: bookstack_packages
    - key_values:
        {%- macro condconf(option) %}
        {%- if option in mypillar -%}
        {%- if ( mypillar[option] is string and mypillar[option].startswith('$') ) or mypillar[option] is number %}
        {%- set value = mypillar[option] %}
        {%- else %}
        {%- set value = "\"'" ~ mypillar[option] ~ "'\"" %}
        {%- endif %}
        {{ option | upper }}: {{ value }}
        {%- endif -%}
        {%- endmacro %}
        {{ condconf('app_url') }}
        {{ condconf('db_host') }}
        {{ condconf('db_database') }}
        {{ condconf('db_username') }}
        {{ condconf('db_password') }}
        {{ condconf('mail_driver') }}
        {{ condconf('mail_from_name') }}
        {{ condconf('mail_from') }}
        {{ condconf('mail_host') }}
        {{ condconf('mail_port') }}
        {{ condconf('mail_username') }}
        {{ condconf('mail_password') }}
        {{ condconf('mail_encryption') }}
        {{ condconf('app_theme') }}
        {{ condconf('cache_driver') }}
        {{ condconf('session_driver') }}
        {{ condconf('memcached_servers') }}
        {{ condconf('session_secure_cookie') }}
        {{ condconf('session_cookie_name') }}
        {{ condconf('app_debug') }}
        {{ condconf('session_lifetime') }}
        {{ condconf('auth_method') }}
        {{ condconf('auth_auto_initiate') }}
        {{ condconf('saml2_name') }}
        {{ condconf('saml2_email_attribute') }}
        {{ condconf('saml2_external_id_attribute') }}
        {{ condconf('saml2_display_name_attributes') }}
        {{ condconf('saml2_idp_entityid') }}
        {{ condconf('saml2_idp_sso') }}
        {{ condconf('saml2_idp_slo') }}
        {{ condconf('saml2_idp_x509') }}
        {{ condconf('saml2_autoload_metadata') }}
        {{ condconf('saml2_sp_x509') }}
        {{ condconf('saml2_user_to_groups') }}
        {{ condconf('saml2_group_attribute') }}
        {{ condconf('saml2_remove_from_groups') }}
        {{ condconf('saml2_dump_user_details') }}
        {{ condconf('queue_connection') }}
        {{ condconf('app_views_books') }}
{%- endif %}
