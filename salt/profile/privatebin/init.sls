{%- set mypillar = salt['pillar.get']('profile:privatebin', {}) -%}
{%- set confdir = '/etc/PrivateBin' -%}
{%- set configfile = confdir ~ '/conf.php' -%}

privatebin_packages:
  pkg.installed:
    - names:
      - PrivateBin-config-httpd

privatebin_clean:
  file.directory:
    - name: {{ confdir }}
    - clean: True
    - onchanges:
      - pkg: privatebin_packages
    - require:
      - pkg: privatebin_packages

{%- if mypillar | length %}
{{ configfile }}:
  ini.options_present:
    - separator: '='
    - strict: True
    - sections:
        {%- macro conf(section, options) %}
        {%- for option in options.keys() -%}
        {%- if ( mypillar[section][option] is string and mypillar[section][option].startswith('$') ) or mypillar[section][option] is number %}
        {%- set value = mypillar[section][option] -%}
        {%- else %}
        {%- set value = "\"'" ~ mypillar[section][option] ~ "'\"" -%}
        {%- endif %}
          {{ option }}: {{ value }}
        {%- endfor -%}
        {%- endmacro %}
        {%- for section, options in mypillar.items() %}
        {{ section }}:
          {{ conf(section, options) }}
        {%- endfor %}
    - require:
      - pkg: privatebin_packages
    - watch:
      - file: privatebin_clean
    - watch_in:
      - file: privatebin_permissions
{%- endif %}

privatebin_permissions:
  file.managed:
    - mode: '0640'
    - user: wwwrun
    - group: privatebin
    - names:
      - {{ configfile }}
    - require:
      - pkg: privatebin_packages
