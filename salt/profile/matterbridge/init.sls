{%- set mypillar = salt['pillar.get']('profile:matterbridge') -%}
{%- set instances = mypillar['instances'] | default([]) -%}

matterbridge_packages:
  pkg.installed:
    - pkgs:
      - matterbridge

matterbridge_directory:
  file.directory:
    - user: root
    - group: matterbridge
    - clean: True
    - require:
      - pkg: matterbridge_packages
{%- if instances | length > 0 %}
      - file: matterbridge_files
{%- endif %}
    - names:
      - /etc/matterbridge

{%- if instances | length > 0 %}
matterbridge_files:
  file.managed:
    - user: root
    - mode: '0644'
    - template: jinja
    - source: salt:///{{ slspath }}/files/etc/matterbridge/matterbridge.toml.j2
    - names:
{%- for instance in instances %}
      - /etc/matterbridge/{{ instance }}.toml:
        - context:
            instance: {{ instance }}
            general: {{ instances[instance]['general'] | default({}) }}
            accounts: {{ instances[instance]['accounts'] }}
            gateways: {{ instances[instance]['gateways'] }}
        - watch_in:
          - service: matterbridge_{{ instance }}_service
{%- endfor %}

{%- for instance in instances %}
{%- if 'general' in instances[instance] and 'MediaDownloadPath' in instances[instance]['general'] %}
matterbridge_{{ instance }}_mediadir:
  file.directory:
    - name: {{ instances[instance]['general']['MediaDownloadPath'] }}
    - user: matterbridge
    {#- to-do: implement some shared group #}
    - group: lighttpd
    - mode: '0750'
    - makedirs: True
{%- endif %}

matterbridge_{{ instance }}_service:
  service.running:
    - name: matterbridge@{{ instance }}.service
    - enable: True
    - watch:
      - file: /etc/matterbridge/{{ instance }}.toml
{%- endfor %}
{%- endif %}

matterbridge_cleanup_timer:
  service.running:
    - name: matterbridge-cleanup.timer
    - enable: True
