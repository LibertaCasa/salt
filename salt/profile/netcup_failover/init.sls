include:
  - profile.keepalived_script_user

/usr/local/bin/failover:
  file.managed:
    - user: keepalived_script
    - group: wheel
    - mode: '0750'
    - template: jinja
    - source: salt://{{ slspath }}/files/failover.sh.j2
