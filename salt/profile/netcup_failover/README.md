This profile installs a script switching failover IP addresses between Netcup hosted VM's.

Required pillar:

```
profile:
  netcup_failover:
    scp_user: 12345
    scp_pass: xxxx
    scp_server: v9876
    mac_address: ff:ff:ff:ff:ff
    ip4_address: xx.xx.xx.xx/32
    ip6_address: 'foo:bar::/64'
```
