{%- set salt_pillar = pillar['salt'] -%}
{%- set master_pillar = salt_pillar['master'] -%}
{%- set redis_config = '/etc/redis/salt.conf' -%}
{%- set redis_service = 'redis@salt' -%}
{%- set extmods = salt_pillar['extmods'] -%}
{%- set extmoddir = master_pillar['extension_modules'] -%}

include:
  - salt.master
  - .formulas

salt_master_extension_modules_dirs:
  file.directory:
    - names:
      - {{ extmoddir }}
      {%- for modtype in extmods %}
      - {{ extmoddir }}/{{ modtype }}
      {%- endfor %}
    - user: root
    - group: salt
    - mode: '0755'

salt_master_extension_modules_bins:
  file.managed:
    - names:
      {%- for modtype, modnames in extmods.items() %}
      {%- for modname in modnames %}
      - {{ extmoddir }}/{{ modtype }}/{{ modname }}:
        - source: salt://extmods/{{ modtype }}/{{ modname }}
      {%- endfor %}
      {%- endfor %}
    - user: root
    - group: salt
    - mode: '0640'
    - require:
      - file: salt_master_extension_modules_dirs

salt_master_extra_packages:
  pkg.installed:
    - names:
      - python3-ldap
      - python3-pynetbox
      - python3-redis
      - redis7
      - salt-bash-completion
      - salt-fish-completion
      - salt-keydiff
    - watch_in:
      - service: salt-master

# to-do: move Redis configuration to a formula
{{ redis_config }}:
  file.managed:
    - contents:
      - port 0
      - tcp-backlog 511
      - unixsocket /run/redis/salt.sock
      - unixsocketperm 460
      - timeout 0
      - supervised systemd
      - pidfile /run/redis/salt.pid
      - logfile /var/log/redis/salt.log
      - databases 1
      - dir /var/lib/redis/salt/
      - acllog-max-len 64
      - requirepass {{ master_pillar['cache.redis.password'] }}
    - user: root
    - group: redis
    - mode: '0640'
    - require:
      - pkg: redis7

/var/lib/redis/salt:
  file.directory:
    - user: redis
    - group: redis
    - mode: '0750'
    - require:
      - pkg: redis7

salt_redis_service_enable:
  service.enabled:
    - name: {{ redis_service }}
    - require:
      - pkg: redis7

salt_redis_service_start:
  service.running:
    - name: {{ redis_service }}
    - require:
      - pkg: redis7
    - watch:
      - file: {{ redis_config }}

salt_redis_membership:
  group.present:
    - name: redis
    - require:
      - pkg: redis7
    - addusers:
      - {{ master_pillar['user'] }}
{%- if pillar['secret_salt'] is defined %}
      {%- for user in master_pillar['publisher_acl'] %}
      - {{ user }}
      {%- endfor %}

admin_salt_membership:
  group.present:
    - name: salt
    - require:
      - pkg: salt-master
    - addusers:
      {%- for user in master_pillar['publisher_acl'] %}
      - {{ user }}
      {%- endfor %}
{%- endif %}
