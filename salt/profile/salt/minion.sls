{%- set netbox_pillar = salt['pillar.get']('netbox') -%}
{%- if 'custom_fields' in netbox_pillar
    and netbox_pillar['custom_fields']['salt_roles'] is not none
    and 'salt.syndic' in netbox_pillar['custom_fields']['salt_roles'] -%}
{%- set master = salt['pillar.get']('salt:master:syndic_master') -%}
{%- elif 'config_context' in netbox_pillar -%}
{%- set master = netbox_pillar['config_context']['salt_master'] -%}
{%- else -%}
{%- do salt.log.warning('Could not determine Salt master') -%}
{%- set master = 'FIX-ME.lysergic.dev' -%}
{%- endif -%}

/etc/salt/minion.d/master.conf:
  file.managed:
    - contents:
      - 'master: {{ master }}'
    - require_in:
      - service: salt-minion
    - watch_in:
      - service: salt-minion

include:
  - salt.minion
