salt_master_formulas:
  git.latest:
    - name: https://git.com.de/LibertaCasa/salt-formulas.git
    - target: /srv/formulas
    - branch: production
    - submodules: True
