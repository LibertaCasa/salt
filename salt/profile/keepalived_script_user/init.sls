keepalived_script_user:
  user.present:
    - name: keepalived_script
    - createhome: False
    - home: /var/lib/keepalived
    - shell: /usr/sbin/nologin
    - system: True
