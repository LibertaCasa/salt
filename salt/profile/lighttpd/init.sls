{%- set mypillar = 'profile:lighttpd' -%}
{%- set vhosts = salt['pillar.get'](mypillar ~ ':vhosts') or [] -%}

lighttpd_packages:
  pkg.installed:
    - pkgs:
      - lighttpd

{%- if vhosts | length > 0 %}
lighttpd_directories:
  file.directory:
    - user: root
    - group: lighttpd
    - mode: '0750'
    - clean: True
    - require:
      - pkg: lighttpd_packages
      - file: lighttpd_files
    - names:
      - /etc/lighttpd/vhosts.d

lighttpd_files:
  file.managed:
    - user: root
    - group: lighttpd
    - mode: '0640'
    - template: jinja
    - watch_in:
      - service: lighttpd_service
    - names:
      - /etc/lighttpd/lighttpd.conf:
        - source: salt:///{{ slspath }}/files/etc/lighttpd/lighttpd.conf.j2
{%- for vhost, config in vhosts.items() %}
      - /etc/lighttpd/vhosts.d/{{ vhost }}.conf:
        - source: salt:///{{ slspath }}/files/etc/lighttpd/vhosts.conf.j2
        - context:
            vhostconfig: {{ config }}
{%- endfor %}
{%- endif %}

lighttpd_service:
  service.running:
    - name: lighttpd.service
    - enable: True
    - reload: True
    - require:
      - pkg: lighttpd_packages
