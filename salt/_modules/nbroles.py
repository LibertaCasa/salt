#to-do: tidy this up, make host configurable

import requests

host = 'http://127.0.0.1:5000/roles?machine='

def get(name):
    req = requests.get(host + name)
    if req.status_code == 404:
        return([])
    if req.status_code == 200:
        return(req.json()['roles'])
