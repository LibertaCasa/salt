{%- set id = salt['grains.get']('id') -%}
{%- set roles = salt['http.query']('http://machine-roles.lysergic.dev:4580/roles', decode=True, decode_type='json', params={"machine": id})['dict']['roles'] -%}

{{ saltenv }}:
  '*':
    - common
  {%- if roles | length > 0 %}
  '{{ id }}':
  {% for role in roles %}
    - role.{{ role }}
  {%- endfor %}
  {%- endif %}
