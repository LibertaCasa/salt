include:
  - openssh.banner
{%- if salt['pillar.get']('manage_sshd', True) %}
  - openssh.config

/etc/ssh/user_ca:
  file.managed:
    - contents:
      {%- for key in salt['pillar.get']('secret_ssh:userca_keys') -%}
      - {{ key }}
      {%- endfor -%}
    - require:
      - pkg: openssh
{%- endif %}
