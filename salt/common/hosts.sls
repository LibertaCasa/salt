# this is a hack because we currently only allow email relaying using the split-horizon zz0.email
selene-hosts:
  host.present:
    - comment: Needed for email
    - ip: 192.168.0.120
    - names:
      - selene.psyched.dev
      - selene
      - zz0.email
