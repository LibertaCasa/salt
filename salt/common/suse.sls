include:
  {#- drop pillar check after all firewall configurations have been imported #}
  {%- if salt['pillar.get']('manage_firewall', False) %}
  - firewalld
  {%- endif %}
  - profile.seccheck
  - profile.zypp
  - profile.prometheus.node_exporter
  {%- if salt['cmd.run']("awk '/^passwd/{ print $2; exit }' /etc/nsswitch.conf") == 'sss' %}
  {%- do salt.log.warning('Not configuring local users due to sss') %}
  {%- else %}
  - users
  {%- endif %}
  - .ssh
  - postfix.config

{#- to-do: move this to some formula or macro -#}
{%- set osfullname = grains['osfullname'] -%}
{#- this SLES clause likely only works in BCI -#}
{%- if osfullname == 'Leap' or osfullname == 'SLES' -%}
{%- set repoos = grains['osrelease'] -%}
{%- elif osfullname == 'openSUSE Tumbleweed' -%}
{%- set repoos = 'openSUSE_Tumbleweed' -%}
{%- else -%}
{%- do salt.log.error('Unsupported operating system.') -%}
{%- endif -%}
{%- set repobase = 'https://download.opensuse.org/repositories/home:/crameleon:/LibertaCasa/' ~ repoos -%}
{%- set repokey = repobase ~ '/repodata/repomd.xml.key' %}

libertacasa_rpm_key:
  cmd.run:
    - name: rpm --import {{ repokey }}
    - unless: rpm -q --quiet gpg-pubkey-f8722274

libertacasa_rpm_repository:
  pkgrepo.managed:
    - name: 'LibertaCasa'
    - baseurl: {{ repobase }}
    - gpgcheck: 1
    - gpgkey: {{ repokey }}
    - priority: 90
    - refresh: True
    - require:
      - cmd: libertacasa_rpm_key

ca-certificates-syscid:
  pkg.installed:
    - require:
      - pkgrepo: libertacasa_rpm_repository

common_packages_install:
  pkg.installed:
    - names:
      - fish
      - system-group-wheel
{%- if grains['virtual'] == 'kvm' %}
      - qemu-guest-agent

qemu-guest-agent:
  service.running:
    - enable: True
    - require:
      - pkg: qemu-guest-agent
{%- endif %}

common_packages_remove:
  pkg.removed:
    - pkgs:
      {#- we only use AutoYaST for the OS deployment #}
      - autoyast2
      - autoyast2-installation
      - yast2-add-on
      - yast2-services-manager
      - yast2-slp
      - yast2-trans-stats
