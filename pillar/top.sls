{%- set id = salt['grains.get']('id') -%}
{%- set roles = salt['http.query']('http://machine-roles.lysergic.dev:4580/roles', decode=True, decode_type='json', params={"machine": id})['dict']['roles'] -%}

{{ saltenv }}:
  '*':
    - global
  'id:{{ id }}':
    - match: grain
    - ignore_missing: True
    - id.{{ id.replace('.', '_') }}
{%- if roles | length > 0 %}
  '{{ id }}':
  {% for role in roles %}
    - ignore_missing: True
    - role.{{ role }}
  {%- endfor %}
{%- endif %}
