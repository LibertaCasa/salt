prometheus:
  wanted:
    component:
      - blackbox_exporter
  pkg:
    component:
      blackbox_exporter:
        config:
          modules:
            http_2xx:
              prober: http
              timeout: 15s
            http_post_2xx:
              prober: http
              http:
                method: POST
            http_3xx:
              prober: http
              timeout: 5s
              http:
                method: HEAD
                no_follow_redirects: true
                valid_status_codes: [301, 302]
            tcp_connect:
              prober: tcp
            ssh_banner:
              prober: tcp
              tcp:
                query_response:
                - expect: "^SSH-2.0-"
            irc_banner:
              prober: tcp
              tcp:
                query_response:
                - send: "NICK prober"
                - send: "USER prober prober prober :prober"
                - expect: "PING :([^ ]+)"
                  send: "PONG ${1}"
                - expect: "^:[^ ]+ 001"
            icmp:
              prober: icmp

firewalld:
  zones:
    internal:
      ports:
        - comment: 'Prometheus Blackbox Exporter'
          port: 9115
          protocol: tcp

