prometheus:
  wanted:
    component:
      - prometheus
  pkg:
    component:
      prometheus:
        config:
          global:
            scrape_interval: 15s
            evaluation_interval: 1m

firewalld:
  zones:
    internal:
      services:
        - prometheus
