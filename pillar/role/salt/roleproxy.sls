salt:
  roleproxy:
    nb_host: ${'secret_salt:roleproxy:nb_host'}
    nb_token: ${'secret_salt:roleproxy:nb_token'}

firewalld:
  zones:
    internal:
      ports:
        - comment: salt-roleproxy
          port: 4580
          protocol: tcp
