salt:
  clean_config_d_dir: False
  minion_remove_config: True
  minion:
    master_type: str
    backup_mode: minion
    cache_jobs: True
    enable_gpu_grains: False
  saltenv: production
