{%- from 'map.jinja' import nginx_crtkeypair -%}
{%- set trustcrt = '/usr/share/pki/trust/anchors/syscid-ca.crt' -%}
{%- set stapler = 'http://gaia.syscid.com:8900/' -%}
{%- set resolver = '192.168.0.115' -%}
{%- set mailer = '192.168.0.120' -%}
{%- set ha4 = '81.16.19.62' -%}
{%- set ha6 = '2a03:4000:20:21f::' -%}

keepalived:
  config:
    global_defs:
      notification_email:
        - system@lysergic.dev
      notification_email_from: failover@{{ grains['host'] }}.lysergic.dev
      smtp_server: {{ mailer }}
      smtp_connect_timeout: 30
      router_id: SSO_FO
      enable_script_security: true
    vrrp_script:
      check_nginx_port:
        script: '"/usr/bin/curl -kfsSm2 https://[::1]:443"'
        weight: 5
        interval: 3
        timeout: 3
      check_nginx_process:
        {#- this is not a good check but better than nothing #}
        script: '"/usr/bin/pgrep nginx"'
        weight: 4
        interval: 2
        timeout: 10
      check_useless_process:
        {#- this is only used for debugging #}
        script: '"/usr/bin/pgrep useless.sh"'
        weight: 4
        interval: 2
        timeout: 3
    vrrp_instance:
      DENCWC:
        state: MASTER
        interface: eth1
        priority: 100
        virtual_router_id: 100
        advert_int: 5
        smtp_alert: true
        notify_master: '"/usr/local/bin/failover --all"'
        promote_secondaries: true
        mcast_src_ip: 192.168.0.50
        authentication:
          auth_type: PASS
          auth_pass: ${'secret_keepalived:vrrp_instance:DENCWC'}
        virtual_ipaddress:
          - {{ ha4 }}/32 dev eth0 label failover
        virtual_ipaddress_excluded:
          - {{ ha6 }}/64 dev eth0
          {%- for i in [1, 2, 3] %}
          - {{ ha6 }}{{ i }}/64 dev eth0
          {%- endfor %}
        track_script:
          {#- - check_nginx_port # to-do: this is currently bugged, check script locks up #}
          - check_nginx_process
        track_interface:
          - eth0

nginx:
  snippets:
    listen_ha:
      - listen:
        - {{ ha4 }}:443 ssl http2
        - '[{{ ha6 }}]:443 ssl http2'
    proxy:
      - proxy_set_header:
        - Host                $host
        - X-Real-IP           $remote_addr
        - X-Forwarded-For     $proxy_add_x_forwarded_for
        - X-Forwarded-Host    $host
        - X-Forwarded-Server  $host
        - X-Forwarded-Port    $server_port
        - X-Forwarded-Proto   $scheme
      - proxy_ssl_trusted_certificate: /etc/pki/trust/anchors/backend-ca.crt
    tls:
      - ssl_ciphers: ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384

    {#- certificate snippets, to-do: merge snippets/tls include into crtkeypair #}
    {{ nginx_crtkeypair('libertacasa', 'liberta.casa') | indent }}
      - include: snippets/tls
    {{ nginx_crtkeypair('libertacasanet', 'libertacasa.net') | indent }}
      - include: snippets/tls
    {{ nginx_crtkeypair('libsso', 'libsso.net') | indent }}
      - include: snippets/tls
    {{ nginx_crtkeypair('lysergic', 'lysergic.dev') | indent }}
      - include: snippets/tls
    tls_syscidsso:
      - ssl_client_certificate:  {{ trustcrt }}
      - ssl_certificate:         /etc/ssl/syscid/sso.syscid.com.crt
      - ssl_certificate_key:     /etc/ssl/syscid/sso.syscid.com.key
      - ssl_ocsp:                'on'
      - ssl_ocsp_responder:      {{ stapler }}
      - ssl_stapling_responder:  {{ stapler }}
      - ssl_verify_client:       'on'
      - resolver:                {{ resolver }} ipv6=off
      - include:                 snippets/tls

  servers:
    managed:
      jboss-cluster.conf:
        available_dir: /etc/nginx/conf.d
        config:
        - proxy_cache_path: /var/cache/nginx/sso_public keys_zone=cache_sso_public:10m
        - proxy_cache_path: /var/cache/nginx/sso_private keys_zone=cache_sso_private:10m
        - upstream jboss:
          - ip_hash: ''
          - server:
            - theia.backend.syscid.com:8443
            - orpheus.backend.syscid.com:8443
            - selene.backend.syscid.com:8443

      bookstack.conf:
        config:
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_libertacasa
            - server_name: libertacasa.info libcasa.info
            - location /:
              - proxy_pass: https://bookstack.themis.backend.syscid.com
              - proxy_http_version: 1.1
            - client_max_body_size: 20M
            - modsecurity_rules: |-
                '
                SecRuleRemoveById 941160 949110
                SecAction "id:900200, phase:1, nolog, pass, t:none, setvar:\'tx.allowed_methods=GET HEAD POST OPTIONS PUT PATCH\'"
                '

      http.conf:
        config:
          - server:
            - listen:
              - {{ ha4 }}:80 default_server
              - '[{{ ha6 }}]:80 default_server'
              - include: snippets/robots
              - location /:
                - return: 301 https://$host$request_uri

      privatebin.conf:
        config:
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_lysergic
            - server_name: pasta.lysergic.dev
            - location /:
              - proxy_pass: https://privatebin.themis.backend.syscid.com
              - proxy_http_version: 1.1
            - client_max_body_size: 50M
            - modsecurity_rules: |-
                '
                SecRequestBodyNoFilesLimit 50000000
                '

      sso_private.conf:
        config:
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_syscidsso
            - server_name: sso.syscid.com
            - root: /srv/www/sso.syscid.com
            - location = /: []
            - location /index.html: []
            - location /:
              - proxy_pass: https://jboss
              - proxy_cache: cache_sso_private
              - include: snippets/proxy
            - proxy_buffer_size: 256k
            - proxy_buffers: 4 512k
            - proxy_busy_buffers_size: 512k
            - error_log: /var/log/nginx/sso_private.error.log
            - access_log: /var/log/nginx/sso_private.access.log combined

      sso_public.conf:
        config:
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_libsso
            - server_name: sso.casa www.sso.casa
            - location /:
              - root: /srv/www/sso.casa
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_libsso
            - server_name: libsso.net www.libsso.net
            - location /:
              - root: /srv/www/libsso.net
            - location /auth: {#- compat, consider removing #}
              - rewrite: '^/auth(.*)$ https://libsso.net$1 break'
            {%- for path in ['realms', 'resources', 'js'] %}
            - location /{{ path }}:
              - proxy_pass: https://jboss/{{ path }}
              - proxy_cache: cache_sso_public
              {#- - proxy_ssl_verify: on #to-do: enable this #}
              - include: snippets/proxy
            {%- endfor %}
            {%- for path in ['admin', 'welcome', 'metrics', 'health' ] %}
            - location /{{ path }}:
              - return: https://liberta.casa/
            {%- endfor %}
            - proxy_buffer_size: 256k
            - proxy_buffers: 4 512k
            - proxy_busy_buffers_size: 512k
            - error_log: /var/log/nginx/libsso_public.error.log
            - access_log: /var/log/nginx/libsso_public.access.log combined

      agola.conf:
        config:
          - server:
            - include:
              - snippets/listen_ha
              - snippets/tls_lysergic
            - server_name: ci.lysergic.dev ci.git.com.de
            - location /:
              - proxy_pass: https://ci.lysergic.dev
              - proxy_ssl_verify: 'on'
              - include: snippets/proxy

manage_firewall: True
firewalld:
  zones:
    public:
      services:
        - http
        - https

profile:
  apparmor:
    local:
      usr.sbin.nginx:
        - '{{ trustcrt }} r,'
        - '/srv/www/{libsso.net,sso.casa,sso.syscid.com}/{index.html,stuff/tacit-css-1.5.2.min.css} r,'
