manage_firewall: True
firewalld:
  zones:
    public:
      services:
        - http
        - https
