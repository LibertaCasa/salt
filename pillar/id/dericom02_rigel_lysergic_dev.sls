{%- set mediapath = '/var/lib/matterbridge/' -%}

{%- macro discord_common() -%}
            AutoWebhooks: 'true'
            EditSuffix: '(edited)'
            RemoteNickFormat: '[{PROTOCOL}]:{NICK} '
{%- endmacro -%}

profile:
  matterbridge:
    instances:
      libertacasa-general:
        general:
          MediaDownloadSize: 1000000000
          MediaDownloadPath: {{ mediapath }}libertacasa-general
          MediaServerDownload: https://load.casa
        accounts:
          irc.libertacasa:
            Server: irc.liberta.casa:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Nick: viaduct
            NickServNick: viaduct
            NickServPassword: ${'secret_matterbridge:general:accounts:irc.libertacasa:NickServPassword'}
            ColorNicks: 'true'
            Charset: utf8
            MessageSplit: 'true'
            MessageQueue: 60
            UseRelayMsg: 'true'
            RemoteNickFormat: '{NICK}/{LABEL}'
          xmpp.libertacasa:
            Server: xmpp.liberta.casa:5222
            Jid: viaduct@liberta.casa
            Password: ${'secret_matterbridge:general:accounts:xmpp.libertacasa:Password'}
            Muc: muc.liberta.casa
            Nick: viaduct
            RemoteNickFormat: '[{PROTOCOL}] <{NICK}> '
            Label: x
            Debug: 'false'
          telegram.libertacasa:
            Token: ${'secret_matterbridge:general:accounts:telegram.libertacasa:Token'}
            RemoteNickFormat: '[{PROTOCOL}] &lt;{NICK}&gt; '
            MessageFormat: HTMLNick
            Label: tg
            DisableWebPagePreview: 'true'
          sshchat.Psyched:
            Server: 192.168.0.110:2220
            Nick: LC
            RemoteNickFormat: '{PROTOCOL}:<{NICK}> '
            Label: ssh
          discord.23:
            Token: ${'secret_matterbridge:general:accounts:discord.23:Token'}
            Server: ${'secret_matterbridge:general:accounts:discord.23:Server'}
            {{ discord_common() }}
          {#-
          discord.aithunder:
            Token: ${'secret_matterbridge:general:accounts:discord.aithunder:Token'}
            Server: ${'secret_matterbridge:general:accounts:discord.aithunder:Server'}
            {{ discord_common() }}
          #}
        gateways:
          libcasa:
            irc.libertacasa: '#libcasa'
            xmpp.libertacasa: libcasa
          dev:
            irc.libertacasa: '#dev'
            xmpp.libertacasa: dev
          lucy:
            irc.libertacasa: '#lucy'
            xmpp.libertacasa: lucy
            telegram.libertacasa: '-1001795702961'
            sshchat.Psyched: sshchat
          info:
            irc.libertacasa: '#libcasa.info'
            xmpp.libertacasa: libcasa.info
          chat:
            irc.libertacasa: '#chat'
            discord.23: chat
            xmpp.libertacasa: chat
          petals:
            irc.libertacasa: '#Petals'
            telegram.libertacasa: '-1001971550949'


      libertacasa-irc:
        general:
          RemoteNickFormat: '{NOPINGNICK}/{LABEL}: '
          IgnoreFailureOnStart: 'true'
          MessageSplit: 'true'
          MediaDownloadSize: 1000000000
          MediaDownloadPath: {{ mediapath }}libertacasa-irc
          MediaServerDownload: https://irc.load.casa
        accounts:
          irc.libertacasa:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.libertacasa:NickServPassword'}
            Server: irc.liberta.casa:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: libcasa
            Charset: utf8
            IgnoreNicks: HistServ
            UseRelayMsg: 'true'
            RemoteNickFormat: '{NICK}/{LABEL}'
          irc.chillnet:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.chillnet:NickServPassword'}
            Server: irc.chillnet.org:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: chillnet
            Charset: utf8
            IgnoreNicks: HistServ
            UseRelayMsg: 'true'
            RemoteNickFormat: '{NICK}/{LABEL}'
          irc.ergo:
            Nick: LCIRCrelay
            NickServNick: LCIRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.ergo:NickServPassword'}
            Server: irc.ergo.chat:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: ergochat
            Charset: utf8
            IgnoreNicks: HistServ
            UseRelayMsg: 'true'
            RemoteNickFormat: '{NICK}/{LABEL}'
          irc.2600:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.2600:NickServPassword'}
            Server: irc.2600.net:6697
            UseTLS: 'true'
            SkipTLSVerify: 'true'
            Label: 2600net
            Charset: utf8
          irc.dosers:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.dosers:NickServPassword'}
            Server: irc.dosers.net:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: dosers
            Charset: utf8
          irc.rizon:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.rizon:NickServPassword'}
            Server: irc.rizon.net:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: rizon
            Charset: utf8
          irc.nerds:
            Nick: LCRelay
            NickServNick: LCRelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.nerds:NickServPassword'}
            Server: irc6.irc-nerds.net:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: nerds
            Charset: utf8
          irc.oftc:
            Nick: IRCrelay
            NickServNick: IRCrelay
            Server: irc.oftc.net:6697
            UseTLS: 'true'
            Label: oftc
            Charset: utf8
          irc.libera:
            Nick: IRCrelay
            NickServNick: IRCrelay
            NickServPassword: ${'secret_matterbridge:irc:accounts:irc.libera:NickServPassword'}
            Server: irc.eu.libera.chat:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Label: libera
            Charset: utf8
          irc.stardust:
            Nick: IRCrelay
            Server: irc.stardust.cx:6697
            UseTLS: 'true'
            Charset: utf8
            Label: stardust
            # ugly but requested
            RemoteNickFormat: '[{LABEL}] <{NICK}> '
        gateways:
          main:
            irc.libertacasa: '#libcasa'
            irc.2600: '#libcasa'
            irc.nerds: '#praxis'
            irc.libera: '#libcasa'
            irc.oftc: '#libcasa'
            irc.dosers: '#libcasa'
            irc.rizon: '#praxis'
          lucy:
            irc.libertacasa: '#lucy'
            irc.dosers: '#lucy'
          libcasainfo:
            irc.libertacasa: '#libcasa.info'
            irc.ergo: '#libcasa.info'
            irc.libera: '#libcasa.info'
            irc.oftc: '#libcasa.info'
          ircv5:
            irc.libertacasa: '#ircv5'
            irc.libera: '#ircv5'
            irc.oftc: '#ircv5'
          nerds:
            irc.libertacasa: '#nerds'
            irc.nerds: '#nerds'
          music:
            irc.libertacasa: '#music'
            irc.chillnet: '#music'
            irc.stardust: '#music'
      chillnet:
        general:
          MediaDownloadSize: 1000000000
          MediaDownloadPath: {{ mediapath }}chillnet
          MediaServerDownload: https://up.chillnet.org
        accounts:
          irc.chillnet:
            Server: irc.chillnet.org:6697
            UseTLS: 'true'
            UseSASL: 'true'
            Nick: viaduct
            NickServNick: viaduct
            NickServPassword: ${'secret_matterbridge:chillnet:accounts:irc.chillnet:NickServPassword'}
            ColorNicks: 'true'
            Charset: utf8
            MessageSplit: 'true'
            MessageQueue: 60
            UseRelayMsg: 'true'
            RemoteNickFormat: '{NICK}/{LABEL}'
          telegram.chillnet:
            Token: ${'secret_matterbridge:chillnet:accounts:telegram.chillnet:Token'}
            RemoteNickFormat: '&lt;{NICK}&gt; '
            MessageFormat: HTMLNick
            Label: tg
            DisableWebPagePreview: 'true'
          discord.23:
            Token: ${'secret_matterbridge:general:accounts:discord.23:Token'}
            Server: ${'secret_matterbridge:general:accounts:discord.23:Server'}
            {{ discord_common() }}
        gateways:
          staff:
            irc.chillnet: '#chillstaff'
            telegram.chillnet: '-1001932699309'
          devs:
            irc.chillnet: '#chilldevs'
            telegram.chillnet: '-1001778806358'
            discord.23: chilldevs

  lighttpd:
    vhosts:
      matterbridge-general:
        host: 'libertacasa-general.matterbridge.dericom02.rigel.lysergic.dev'
        root: {{ mediapath }}libertacasa-general
      matterbridge-irc:
        host: 'libertacasa-irc.matterbridge.dericom02.rigel.lysergic.dev'
        root: {{ mediapath }}libertacasa-irc
      matterbridge-chillnet:
        host: 'chillnet.matterbridge.dericom02.rigel.lysergic.dev'
        root: {{ mediapath }}chillnet

manage_firewall: True
firewalld:
  zones:
    web:
      services:
        - http
      sources:
        - '2a01:4f8:11e:2200::dead/128'
