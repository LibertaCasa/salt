{%- set common = {'address': '[fd29:8e45:f292:ff80::1]', 'port': 443, 'domain': '.themis.backend.syscid.com', 'snippetsdir': '/etc/apache2/snippets.d/'} -%}

{%- macro httpdformulaexcess() -%}
      LogLevel: False
      ErrorLog: False
      LogFormat: False
      CustomLog: False
      ServerAdmin: False
      ServerAlias: False
{%- endmacro -%}
{%- macro httpdcommon(app) -%}
        Include {{ common['snippetsdir'] }}ssl_themis.conf
        <FilesMatch '\.php$'>
          SetHandler 'proxy:unix:/run/php-fpm/{{ app }}.sock|fcgi://{{ app }}'
        </FilesMatch>
{%- endmacro -%}

apache:
  sites:
    BookStack:
      interface: '{{ common['address'] }}'
      port: {{ common['port'] }}
      ServerName: bookstack{{ common['domain'] }}
      DocumentRoot: /srv/www/BookStack/
      DirectoryIndex: index.php
      Directory:
        /srv/www/BookStack/:
          Options: FollowSymLinks
          AllowOverride: None
          Require: all granted
          Formula_Append: |
            RewriteEngine On
            RewriteCond %{HTTP:Authorization} .
            RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_URI} (.+)/$
            RewriteRule ^ %1 [L,R=301]
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^ index.php [L]
      {{ httpdformulaexcess() }}
      Formula_Append: |
        {{ httpdcommon('BookStack') }}
        AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript
        SetOutputFilter DEFLATE

    PrivateBin:
      interface: '{{ common['address'] }}'
      port: {{ common['port'] }}
      ServerName: privatebin{{ common['domain'] }}
      DocumentRoot: /srv/www/PrivateBin/public
      DirectoryIndex: index.php
      Directory:
        /srv/www/PrivateBin/:
          Options: false
          AllowOverride: None
          Require: all granted
      {{ httpdformulaexcess() }}
      Formula_Append: |
        {{ httpdcommon('PrivateBin') }}

profile:
  bookstack:
    app_url: https://libertacasa.info
    db_host: ${'secret_bookstack:db_host'}
    db_database: ${'secret_bookstack:db_database'}
    db_username: ${'secret_bookstack:db_username'}
    db_password: ${'secret_bookstack:db_password'}
    mail_driver: smtp
    mail_from_name: LibertaCasa Documentation
    mail_from: mail@libertacasa.info
    mail_host: zz0.email
    mail_port: 465
    mail_username: mail@libertacasa.info
    mail_password: ${'secret_bookstack:mail_password'}
    mail_encryption: ssl
    app_theme: lysergic
    cache_driver: memcached
    session_driver: memcached
    memcached_servers: /run/memcached/memcached.sock
    session_secure_cookie: true
    session_cookie_name: libertacasa_megayummycookie
    app_debug: false
    session_lifetime: 240
    auth_method: saml2
    auth_auto_initiate: true
    saml2_name: LibertaCasa SSO
    saml2_email_attribute: email
    saml2_external_id_attribute: uid
    saml2_display_name_attributes: fullname
    saml2_idp_entityid: https://libsso.net/realms/LibertaCasa
    saml2_idp_sso: https://libsso.net/realms/LibertaCasa/protocol/saml
    saml2_idp_slo: https://libsso.net/realms/LibertaCasa/protocol/saml
    saml2_idp_x509: ${'secret_bookstack:saml2_idp_x509'}
    saml2_autoload_metadata: false
    saml2_sp_x509: ${'secret_bookstack:saml2_sp_x509'}
    saml2_sp_x509_key: ${'secret_bookstack:saml2_sp_x509_key'}
    saml2_user_to_groups: true
    saml2_group_attribute: groups
    saml2_remove_from_groups: true
    queue_connection: database

  privatebin:
    main:
      name: Bin
      fileupload: true
      syntaxhighlightingtheme: sons-of-obsidian
      sizelimit: 310485760
      notice: 'Note: Kittens will die if you abuse this service.'
      languageselection: true
      urlshortener: ${'secret_privatebin:main:urlshortener'}
      qrcode: true
    expire:
      default: 1week
    expire_options:
      5min: 300
      10min: 600
      1hour: 3600
      1day: 86400
      1week: 604800
      1month: 2592000
      1year: 31536000
      never: 0
    formatter_options:
      plaintext: Plain Text
      syntaxhighlighting: Source Code
      markdown: Markdown
    traffic:
      limit: 10
      header: X_FORWARDED_FOR
      dir: /var/lib/PrivateBin/limits
    purge:
      limit: 300
      batchsize: 10
      dir: /var/lib/PrivateBin/limits
    model:
      class: Database
    model_options:
      dsn: ${'secret_privatebin:model_options:dsn'}
      tbl: privatebin_
      usr: ${'secret_privatebin:model_options:usr'}
      pwd: ${'secret_privatebin:model_options:pwd'}
      opt[12]: true

manage_firewall: True
firewalld:
  zones:
    backend:
      services:
        - https
