tor:
  hidden_services:
    http:
      virtport: 80
      target: '[2a01:4f8:11e:2200::dead]:8085'
      hostname: qzzf2qcfbhievvs5nzkccuwddroipy62qjocqtmgcgh75vd6w57m7yad.onion
      hs_ed25519_public_key: PT0gZWQyNTUxOXYxLXB1YmxpYzogdHlwZTAgPT0AAACGcl1ARQnQStZdblQhUsMcXIfj2oJcKE2GEY/+1H63fg==
      hs_ed25519_secret_key: ${'secret_tor:hidden_services:http:key'}
    irc:
      virtport: 6667
      target: '[2a01:4f8:11e:2200::cafe]:6662'
      hostname: cr36xbvmgjwnfw4sly4kuc6c3ozhesjre3y5pggq5xdkkmbrq6dz4fad.onion
      hs_ed25519_public_key: PT0gZWQyNTUxOXYxLXB1YmxpYzogdHlwZTAgPT0AAAAUd+uGrDJs0tuSXjiqC8LbsnJJMSbx15jQ7calMDGHhw==
      hs_ed25519_secret_key: ${'secret_tor:hidden_services:irc:key'}

manage_firewall: True
