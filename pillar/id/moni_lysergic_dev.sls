prometheus:
  pkg:
    component:
      prometheus:
        config:
          alerting:
            alertmanagers:
              - static_configs:
                - targets:
                  - localhost:9093

          rule_files:
            - /etc/prometheus/alerts/lysergic/*.yml

          scrape_configs:
            - job_name: 'prometheus'
              static_configs:
              - targets: ['localhost:9090']

            - job_name: 'node_exporters_lysergic'
              scrape_timeout: 1m
              scrape_interval: 5m
              file_sd_configs:
              - files:
                - '/etc/prometheus/targets/node-lysergic.json'

            - job_name: 'blackbox-2xx'
              metrics_path: /probe
              params:
                module: [http_2xx]
              file_sd_configs:
              - files: ['/etc/prometheus/targets/blackbox-2xx*.json']
              relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: 127.0.0.1:9115

            - job_name: 'blackbox-3xx'
              metrics_path: /probe
              params:
                module: [http_3xx]
              file_sd_configs:
              - files: ['/etc/prometheus/targets/blackbox-3xx*.json']
              relabel_configs:
              - source_labels: [__address__]
                target_label: __param_target
              - source_labels: [__param_target]
                target_label: instance
              - target_label: __address__
                replacement: 127.0.0.1:9115

            - job_name: 'certificate_exporter'
              static_configs:
              - targets: ['therapon.rigel.lysergic.dev:9793']

      alertmanager:
        config:
          route:
            group_by: ['alertname']
            group_wait: 10s
            group_interval: 10s
            repeat_interval: 1h
            receiver: 'smtp-local'
            routes:
            - receiver: 'lysergic'
          #    continue: false
              match:
               project: LYSERGIC
            - receiver: 'chillnet'
              match:
               project: CHILLNET

          receivers:
          - name: 'smtp-local'
            email_configs:
            - to: 'system@lysergic.dev'
              from: 'alertmanager@moni.lysergic.dev'
              require_tls: false
          # !!! TO-DO
              smarthost: 'zz0.email:465'
              send_resolved: yes

          - name: 'irc-libertacasa'
            webhook_configs:
            - url: 'http://127.0.0.1:2410/universe'
              send_resolved: yes

          - name: 'lysergic'
            webhook_configs:
            - url: 'http://127.0.0.1:2410/universe'
              send_resolved: yes
            - url: http://127.0.0.2:8081/prometheus/webhook
              send_resolved: yes
            email_configs:
            - to: 'system@lysergic.dev'
              from: 'alertmanager@moni.lysergic.dev'
              require_tls: false
              smarthost: 'zz0.email:465'
              send_resolved: yes

          - name: 'chillnet'
            email_configs:
            - to: 'team@chillnet.org'
              from: 'alertmanager@moni.lysergic.dev'
              require_tls: false
              smarthost: 'zz0.email:465'
              send_resolved: yes

manage_firewall: True
firewalld:
  zones:
    internal:
      services:
        - https
      ports:
        - comment: DNS Slave
          port: 5353
          protocol: tcp
        - port: 5353
          protocol: udp
