include:
  - secret.id.{{ salt['grains.get']('id').replace('.', '_') }}

profile:
  matterbridge:
    instances:
      libertacasa-general:
        RemoteNickFormat: "{NOPINGNICK}/{LABEL}: "
        IgnoreFailureOnStart: True
        MessageSplit: True
        MediaDownloadPath: /var/lib/matterbridge/libertacasa-general
        MediaServerDownload: "https://dummy.load.casa"
        accounts:
         libertacasa:
          protocol: irc
          Nick: mocker02
          NickServNick: mocker
          Server: 'irc.casa:6697'
          UseTLS: True
          UseSASL: True
          Label: libcasa
          Charset: utf8
          IgnoreNicks: HistServ
          UseRelayMsg: True
          RemoteNickFormat: "{NICK}/{PROTOCOL}-{LABEL}"
         ergo:
          protocol: irc
          Nick: mocker
          Server: 'irc.ergo.chat:6697'
          UseTLS: True
          Label: ergo
          UseRelayMsg: True
        gateways:
         foobar:
           irc.libertacasa: '#dev'
           irc.ergo: '#chat'
  lighttpd:
    vhosts:
      matterbridge-general:
        host: mocker.example.com
        root: /var/lib/matterbridge/libertacasa-general
