#!/usr/bin/env python3

import os
import shutil
import socket

iddir = 'pillar/id/'
secretiddir = 'pillar/secret/id/'
baseidfile = 'pillar/mock/example.sls'
myidfile = socket.gethostname() + '.sls'

shutil.copyfile(baseidfile, os.path.join(iddir, myidfile))
os.makedirs(secretiddir)
shutil.copyfile(baseidfile, os.path.join(secretiddir, myidfile))
