#!/usr/bin/env python3

import os

roles = []
excluded = ['salt.common']

def get():
    for root in ['pillar', 'salt']:
        for rootdir, subdirs, files in os.walk(os.path.join(root, 'role')):
            myrootdir = rootdir.split('/')
            if len(myrootdir) > 2:
                level = myrootdir[2:]
            for file in files:
                splitfile = os.path.splitext(file)
                if len(splitfile) == 2 and splitfile[1] == '.sls':
                    role = os.path.splitext(file)[0]
                    if len(myrootdir) > 2:
                        role = '.'.join(level) + '.' + role
                    if not role in excluded and not role in roles:
                        roles.append(role)
    return roles

if __name__ == '__main__':
    print(get())
