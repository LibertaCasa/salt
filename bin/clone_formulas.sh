#!/bin/sh
set -Ceu

FORMULASDIR='/srv/formulas'

if [ ! -d "$FORMULASDIR" ]
then
        git clone --recurse-submodules --single-branch -b production https://git.com.de/LibertaCasa/salt-formulas.git "$FORMULASDIR"
        exit "$?"
fi

git -C "$FORMULASDIR" pull --recurse-submodules
