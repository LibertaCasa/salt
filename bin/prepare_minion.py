#!/usr/bin/env python3
# Prepares a minion for local testing of Salt code in this repository. Requires roles to be written as grains, as development and pipeline containers generally do not have access to our roles API. Consider nbroles_to_grains.sh first if this is the case.

import roles
import os
import yaml
import socket
import yaml

roles = roles.get()
grainsfile = '/etc/salt/grains'
idfile = os.path.join('pillar/id/', socket.gethostname() + '.sls')
configfile = '/etc/salt/minion.d/local.conf'
mypwd = os.getcwd()
formulasfile = 'formulas.yaml'
formulasdir = '/srv/formulas'

configdata = {'file_roots': {'production': ['/srv/salt']}, 'pillar_merge_lists': True}
grainsdata = {'roles': roles}

with open(grainsfile, mode='w') as grainsfh:
	yaml.dump(grainsdata, grainsfh)

with open(idfile, mode='w') as idfh:
	idfh.write('# empty')

with open(formulasfile, mode='r') as formulasfh:
    formulas = yaml.load(formulasfh, Loader=yaml.FullLoader)

for formula in formulas:
    formula = os.path.join('/srv/formulas/', formula + '-formula')
    configdata['file_roots']['production'].append(formula)

with open(configfile, mode='w') as configfh:
    configfh.write('# written by prepare_minion.py\n')
    yaml.dump(configdata, configfh)

os.symlink(mypwd + '/salt', '/srv/salt')
os.symlink(mypwd + '/pillar', '/srv/pillar')
